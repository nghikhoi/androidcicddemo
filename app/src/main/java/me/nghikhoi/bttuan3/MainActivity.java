package me.nghikhoi.bttuan3;

import android.content.Intent;
import android.os.Build;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.util.HashSet;
import java.util.Set;

import static me.nghikhoi.bttuan3.Utils.dateFromDay;

public class MainActivity extends AppCompatActivity {

    private static final Set<Student> TEST_USERS = new HashSet<Student>() {{
       add(new Student("khoia", "113", "Nghi", "Khoi A", dateFromDay("11/08/2002")));
        add(new Student("khoib", "114", "Nghi", "Khoi B", dateFromDay("11/08/2002")));
        add(new Student("khoic", "115", "Nghi", "Khoi C", dateFromDay("11/08/2002")));
        add(new Student("khoid", "116", "Nghi", "Khoi D", dateFromDay("11/08/2002")));
        add(new Student("khoie", "117", "Nghi", "Khoi E", dateFromDay("11/08/2002")));
    }};

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button btnLogin = findViewById(R.id.btnLogin);
        EditText tbUsername = findViewById(R.id.tbUsername);
        EditText tbPassword = findViewById(R.id.tbPassword);

        btnLogin.setOnClickListener(view -> {
            String toastMessage = "Đăng nhập thất bại";
            Student student = null;

            String username = tbUsername.getText().toString();
            String password = tbPassword.getText().toString();

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.N) {
                student = TEST_USERS.stream().filter(s -> s.getUsername().equals(username) && s.getPassword().equals(password)).findFirst().orElse(null);
            }

            if (student != null) {
                toastMessage = "Đăng nhập thành công";

                Bundle bundle = new Bundle();
                student.setToBundle(bundle);

                Intent intent = new Intent(getApplicationContext(), InformationPage.class);
                intent.putExtra(BundleKey.USER_INFORMATION, bundle);
                startActivity(intent);
            }
            Toast.makeText(getApplicationContext(), toastMessage, Toast.LENGTH_LONG).show();
        });
    }

}
