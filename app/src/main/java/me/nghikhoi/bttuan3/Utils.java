package me.nghikhoi.bttuan3;

import android.annotation.SuppressLint;

import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class Utils {

    @SuppressLint("SimpleDateFormat")
    private static final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd/MM/yyyy");

    public static Date dateFromDay(String day) {
        try {
            return DATE_FORMAT.parse(day);
        } catch (ParseException e) {
            e.printStackTrace();
            return new Date();
        }
    }

}
