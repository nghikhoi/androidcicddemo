package me.nghikhoi.bttuan3;

import android.os.Bundle;

import java.util.Date;

public class Student {

    private final String username;
    private final String password;
    private final String firstName;
    private final String lastName;
    private final Date dayOfBirth;

    public Student(String username, String password, String firstName, String lastName, Date dayOfBirth) {
        this.username = username;
        this.password = password;
        this.firstName = firstName;
        this.lastName = lastName;
        this.dayOfBirth = dayOfBirth;
    }

    public String getUsername() {
        return username;
    }

    public String getPassword() {
        return password;
    }

    public String getFirstName() {
        return firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public Date getDayOfBirth() {
        return dayOfBirth;
    }

    public void setToBundle(Bundle bundle) {
        bundle.putString("firstname", getFirstName());
        bundle.putString("lastname", getLastName());
        bundle.putString("username", getUsername());
        bundle.putString("password", getPassword());
        bundle.putLong("dayofbirth", getDayOfBirth().getTime());
    }

    public static Student fromBundle(Bundle bundle) {
        String firstname = bundle.getString("firstname");
        String lastname = bundle.getString("lastname");
        String username = bundle.getString("username");
        String password = bundle.getString("password");
        Date birthOfDay = new Date(bundle.getLong("dayofbirth"));
        return new Student(username, password, firstname, lastname, birthOfDay);
    }

}
