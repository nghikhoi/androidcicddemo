package me.nghikhoi.bttuan3;

import android.annotation.SuppressLint;
import android.widget.TextView;
import androidx.appcompat.app.AppCompatActivity;
import android.os.Bundle;

import java.text.SimpleDateFormat;

public class InformationPage extends AppCompatActivity {

    @SuppressLint("SimpleDateFormat")
    private final SimpleDateFormat DATE_FORMAT = new SimpleDateFormat("dd-MM-yyyy");

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_information_page);

        Bundle bundle = getIntent().getBundleExtra(BundleKey.USER_INFORMATION);
        TextView lblUsername = findViewById(R.id.lblUsername);
        TextView lblName = findViewById(R.id.lblName);
        TextView lblDayOfBirth = findViewById(R.id.lblDayOfBirth);

        Student student = Student.fromBundle(bundle);
        lblUsername.setText(student.getUsername());
        lblName.setText(student.getFirstName() + " " + student.getLastName());
        lblDayOfBirth.setText(DATE_FORMAT.format(student.getDayOfBirth()));
    }

}
